<?php

namespace App;

use App\Opponent;
use App\BattleHistory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deck extends Model
{
    //
    protected $fillable = [
        'url','release',
    ];

    use SoftDeletes;

    public function deckStore($request,$game_id,$flag){
        //ゲームデータを保存
        $this->fill($request->all());
        $this->deck_name = $request->name;
        $this->leader_id = $request->leader;
        $this->format_id = $request->field;
        if($flag == 0){
            $this->user_id = Auth::id();
            $this->game_id = $game_id;
            $this->season_id = $request->season;
        }
        $this->save();
        //保存したデータのidを取得
        return $this->id;
    }

    public function editDeck($user_id,$game_id,$flag){
        //デッキデータの取得
        if($flag == 0){
            $decks = Deck::where('user_id',$user_id)
              ->where("game_id",$game_id)
              ->orderBy("created_at","desc")->get();
        }else if($flag == 1){
            $decks = $this->where("user_id",$user_id)
              ->where("game_id",$game_id)
              ->where("favorite_deck_id",1)->get();
        }


        //battlehistoryのモデル取得
        $db_battle_history = new BattleHistory;
        $db_leader = new Leader;
        $db_format = new Format;

        foreach($decks as $deck){
            $battle_history = $db_battle_history->getWinRate($deck->id);
            $deck->win = $battle_history->win;
            $deck->win_rate = $battle_history->win_rate;
            $deck->leader = Leader::find($deck->leader_id)->leader_name;
            $deck->format = Format::find($deck->format_id)->format_name;
        }

        return $decks;
    }

    public function getFavorite($user_id,$game_id){
        $favorites = $this->where("user_id",$user_id)->where("game_id",$game_id)
          ->where("favorite_deck_id",1)->get();
        foreach($favorites as $favorite){
            $favorite->leader = Leader::find($favorite->leader_id)->leader_name;
            $favorite->format = Format::find($favorite->format_id)->format_name;
        }
        return $favorites;
    }

    public function getDeckDetail($request){
        $deck = $this->where('id',$request->id)->first();
        $deck->leader = Leader::find($deck->leader_id)->leader_name;
        $deck->format = Format::find($deck->format_id)->format_name;
        $deck->battle_history = BattleHistory::where("deck_id",$deck->id)->first();//リレーションを利用できそうだが、やり方がわからない。保留。
        \Log::info($deck->battle_history);
        return $deck;
    }

    public function deleteDeck($request){
        $deck = $this->where('id',$request->id)->first();
        $deck->delete();
    }

    public function getBattleData($seasons){
        $battle_datas = [];
        foreach($seasons as $key=>$season){
            $win = 0;
            $lose = 0;
            //デッキの取得
            $decks = $this->where("season_id",$season->id)->get();
            //battlehistoryからデータを取得
            foreach($decks as $deck){
                $data = BattleHistory::where("deck_id",$deck->id)->first();
                $win += $data->win;
            }
            \Log::info($win);
        }
    }
    // public function Leader(){
    //     return $this->belongTo("App\Leader","leader_id");
    // }
}
