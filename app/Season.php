<?php

namespace App;

use App\Game;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Season extends Model
{
    use SoftDeletes;
    //
    public function store($request){
        $this->season_name = $request->name;
        $this->season_detail = $request->detail;
        $this->user_id = Auth::id();
        $this->game_id = $request->game;
        $this->save();
    }

    // シーズンの取得
    public function getSeason($user_id,$game_id){
        return $this->where("user_id",$user_id)
          ->where("game_id",$game_id)
          ->orderBy("created_at","desc")->get();
    }

    public function getSeasonList($game_id){
        return $this->where("user_id",Auth::id())
          ->where("game_id",$game_id)->orderBy("created_at","desc")->get();
    }

    public function getEditSeason($season_id){
        return $this->where("id",$season_id)->first();
    }

    public function edit($request){
        $season = $this->where("id",$request->season_id)->first();
        $season->season_name = $request->name;
        $season->season_detail = $request->detail;
        $season->save();
    }

    public function deleteSeason($season_id){
        $season = $this->where('id',$season_id)->first();
        $season->delete();
    }

    public function searchSeasonList($word,$user_id){
        $db_game = new Game;
        $game = $db_game->getGame($user_id);
        return $this->where("user_id",$user_id)
          ->where("game_id",$game->id)
          ->where("season_name","like","%".$word."%")->get();
    }
}
