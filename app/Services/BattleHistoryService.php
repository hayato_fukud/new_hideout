<?php
namespace App\Services;

use App\Deck;
use App\Opponent;
use App\BattleHistory;
use App\RepresentativeDeck;


class BattleHistoryService{

    //battlehistoryのデータを編集し、表示する
    public function editBattleHistory($leaders,$deck_id){
        //全体をまとめるデータ
        $datas = [];

        $sum = [
            "win" => 0,
            "lose" => 0,
            "first" => 0,
            "second" => 0,
            "win_rate" => 0,
            "strong" => "",
            "wrong" => ""
        ];
        $strong_point = -1;
        $wrong_point = 101;
        //カウンター
        $i = 0;
        // //リーダーごとの戦績をまとめる。
        foreach($leaders as $leader){
            //リーダーごとの対戦デッキを加える
            $target_decks = Opponent::where("leader_id",$leader->id)
              ->where("deck_id",$deck_id)->get();
            //対戦デッキごとにデータを作成する。（別の部分で）
            $target_data = $this->editOpponentData($target_decks,$leader->id,$deck_id);
            //sumデータを作成する。
            $sum["win"] += $target_data["win"];
            $sum["lose"] += $target_data["lose"];
            $sum["first"] += $target_data["first"];
            $sum["second"] += $target_data["second"];
            //強いリーダの設定
            if($strong_point < $target_data["win_rate"]){
                $sum["strong"] = $leader->leader_name;
                $strong_point = $target_data["win_rate"];
            }else if($strong_point == $target_data["win_rate"]){
                $sum["strong"] = $sum["strong"] . "、" . $leader->leader_name;
            }
            //弱いリーダーの設定
            if($wrong_point > $target_data["win_rate"]){
                $sum["wrong"] = $leader->leader_name;
                $wrong_point = $target_data["win_rate"];
            }else if($wrong_point == $target_data["win_rate"]){
                $sum["wrong"] = $sum["wrong"] . "、" . $leader->leader_name;
            }
            //指定のdatasの場所に登録
            $datas[$i] = $target_data;
            //カウントを+1する
            $i++;
        }
        //win_rateの算出
        if($sum["win"]+$sum["lose"] != 0){
            $sum["win_rate"] = round($sum["win"] / ($sum["win"] + $sum["lose"]) * 100,2);
        }

        return [$datas,$sum];
    }

    public function editOpponentData($target_decks,$leader_id,$deck_id){
        $win_rate = 0;
        $win = 0;
        $lose = 0;
        $first = 0;
        $second = 0;
        //デッキごとの処理
        foreach($target_decks as $deck){
            //勝敗のカウント
            if($deck->win_or_lose == 0){
                $win+=1;
            }else{
                $lose+=1;
            }
            //先攻・後攻のカウント
            if($deck->order == 0){
                $first+=1;
            }else{
                $second+=1;
            }
        }

        //勝率の算出
        if($win+$lose != 0){
            $win_rate = round(($win / ($win + $lose) ) * 100,2);
        }else{
            $win_rate = 0;
        }

        //代表デッキの取得
        $rep_data = RepresentativeDeck::where("leader_id",$leader_id)
          ->where("deck_id",$deck_id)->first();

        //データの整理
        return array(
            "win"=>$win,
            "lose"=>$lose,
            "win_rate"=>$win_rate,
            "first"=>$first,
            "second"=>$second,
            "representative_deck"=>$rep_data ? $rep_data->representative_deck_name : "",
            "memo"=>$rep_data ? $rep_data->memo : ""
        );
    }

    //シーズンごとの総合得点を取得し、計算する
    public function editAllBattleHistory($season_id,$format_id){
        //まとめるデータ
        $sum = [
            "win" => 0,
            "lose" => 0,
            "first" => 0,
            "second" => 0,
            "win_rate" => 0,
        ];
        //シーズンで使われたデッキを取得
        if($format_id == 0){
            $decks = Deck::where("season_id",$season_id)->get();
        }else{
            $decks = Deck::where("season_id",$season_id)->where("format_id",$format_id)->get();
        }

        //デッキごとに足し合わせる
        foreach($decks as $deck){
            $battle_data = BattleHistory::where("deck_id",$deck->id)->first();
            $sum["win"] += $battle_data->win;
            $sum["lose"] += $battle_data->lose;
            $sum["first"] += $battle_data->first;
            $sum["second"] += $battle_data->second;
        }

        //勝敗の合計が0じゃないなら勝率を計算
        if(($sum["win"] + $sum["lose"]) != 0){
            $sum["win_rate"] = round($sum["win"] / ($sum["win"] + $sum["lose"]) * 100,2);
        }

        return $sum;
    }

    //battlehistoryのデータを編集する（テーブルの書き換え）
    public function editBattleHistoryData($request){
        $battle_history = BattleHistory::where("deck_id",$request->deck_id)->first();

        //勝敗と順番の追加
        if($request->win_or_lose == 0){
            $battle_history->win += 1;
        }else if($request->win_or_lose == 1){
            $battle_history->lose += 1;
        }

        if($request->order == 0){
            $battle_history->first += 1;
        }else if($request->order == 1){
            $battle_history->second += 1;
        }

        $battle_history->win_rate = round($battle_history->win / ($battle_history->win + $battle_history->lose) * 100,2);

        $battle_history->save();
    }

    public function deleteBattleHistoryData($request,$opponent){
        // $opponent = Opponent::where("id",$request->delete)->first();
        $battle_history = BattleHistory::where("deck_id",$request->deck_id)->first();
        // \Log::info($request);
        // \Log::info($opponent);
        // \Log::info($battle_history);

        if($opponent->win_or_lose == 0){
            $battle_history->win -= 1;
        }else if($opponent->win_or_lose == 1){
            $battle_history->lose -= 1;
        }

        if($opponent->order == 0){
            $battle_history->first -= 1;
        }else if($opponent->order == 1){
            $battle_history->second -= 1;
        }

        if($battle_history->win + $battle_history->lose != 0){
            $battle_history->win_rate = round($battle_history->win / ($battle_history->win + $battle_history->lose) * 100,2);
        }else{
            $battle_history->win_rate = 0;
        }

        $battle_history->save();
    }

}
