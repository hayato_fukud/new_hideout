<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

class Game extends Model
{
    use SoftDeletes;
    //
    public function storeGame($request){
        //ゲームデータを保存
        $this->game_name = $request->name;
        $this->game_detail = $request->detail;
        $this->user_id = Auth::id();
        $this->save();
        //保存したデータのidを取得
        return $this->id;
    }

    //現在登録しているゲームのデータ
    public function getGame($user_id){
        return $this->where("user_id",$user_id)->where("choice",1)->first();
    }

    //ゲーム一覧を手に入れるとき
    public function searchGameList($name,$user_id){
        $games = $this->where("user_id",$user_id)
          ->where("game_name","like","%".$name."%")
          ->orderBy("created_at","desc")->get();
        return $games;
    }

    //編集するゲームデータ
    public function getEditGame($game_id){
        return $this->where("id",$game_id)->first();
    }

    public function edit($request){
        \Log::info($request);
        $game = $this->where("id",$request->game_id)->first();
        $game->game_name = $request->name;
        $game->game_detail = $request->detail;
        $game->save();
    }

    public function deleteGame($game_id){
        $game = $this->where('id',$game_id)->first();
        if($game->choice == 0){
            $game->delete();
        }
    }


    //choiceの変更
    public function changeChoice($game_id,$user_id){
        //既存のchoiceを0にする
        $game = $this->where("user_id",$user_id)->where("choice",1)->first();
        $game->choice = 0;
        $game->save();
        //新しいchoiceを設定する
        $new_game = $this->where("id",$game_id)->first();
        $new_game->choice = 1;
        $new_game->save();
    }



}
