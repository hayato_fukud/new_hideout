<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use App\Game;
use App\Leader;
use App\Format;
use App\Mail\SendPreRegisterMail;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        //ユーザー登録
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $game = new Game;

        $game->game_name = $data['game_name'];
        $game->game_detail = $data['game_detail'];
        $game->user_id = $user->id;
        $game->choice = 1;
        $game->save();

        foreach($data["leaders"] as $leader){
            Leader::insert([
                "leader_name" => $leader,
                "game_id" => $game->id
            ]);
        };

        foreach($data["formats"] as $format){
            Format::insert([
                "format_name" => $format,
                "game_id" => $game->id
            ]);
        };

        return $user;
    }

    public function registerStep2(Request $request){
        //バリデーション
        $this->validator($request->all())->validate();

        $request->flashOnly("name","email","password");

        //フラッシュデータを保存
        $request->session()->reflash();

        //nameとdetailのセッションを持ってた場合、nameとdetailを取得する
        $name = ($request->session()->get("name")) ? $request->session()->get("name") : "";
        $detail = ($request->session()->get("detail")) ? $request->session()->get("detail") : "";

        $user = $request->all();

        return view('auth.register_step2',compact("user","name","detail"));
    }

    public function registerStep3(Request $request){
        //flashデータを保存
        $request->session()->reflash();
        // $request->flashOnly('name','email','password','game_name','game_detail');

        $leaders = ($request->session()->get("leaders")) ? $request->session()->get("leaders") : [];
        //新規登録の場合のようにflagを0にする
        // $flag = config("const.Game_Register.New_User");
        $flag = 0;

        return view('auth.register_step3',compact("leaders","flag"));
    }

    public function registerStep4(Request $request){
        //flash data
        $request->session()->reflash();

        $formats = ($request->session()->get("formats")) ? $request->session()->get("formats") : [];
        //新規登録の場合、flagを0にする
        // $flag = config("const.Game_Register.New_User");
        $flag = 0;

        return view('auth.register_step4',compact("formats","flag"));
    }

    public function confirm(Request $request){

        //flash data
        $request->session()->reflash();

        $data = $request->session()->all();
        $password_mask = str_repeat ( "*" , mb_strlen(\old('password')) );
        \Log::info($data);
        return view('auth.confirm',compact("data","password_mask"));
    }

    public function register(Request $request)
    {
        // event(new Registered($user = $this->create( $request->all() )));
        $this->create($request->all());
        return view('auth.registered');
    }

}
