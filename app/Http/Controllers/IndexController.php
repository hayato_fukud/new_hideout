<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndexController extends Controller
{
  public function index(){

    $datas = [
        (object)[
            "deck" => (object)[
                "name" => "レックステリー",
                "leader" => "テリー",
                "type" => "アグロ",
                "win_percent" => "50",
                "win_number" => "46",
                "strong" => "ゼシカ",
            ],
            "compe" => (object)[
                "name" => "えだまめの大会",
                "style" => "トーナメント",
                "battle" => "BO3",
                "date" => "5月15日",
                "place" => "オンライン",
                "description" => "楽しい大会にしましょう！"
            ],
        ],
        (object)[
            "deck" => (object)[
                "name" => "アンルシアカミュ",
                "leader" => "カミュ",
                "type" => "ミッドレンジ",
                "win_percent" => "80",
                "win_number" => "1000",
                "strong" => "ゼシカ",
            ],
            "compe" => (object)[
                "name" => "えだまめの大会",
                "style" => "トーナメント",
                "battle" => "BO3",
                "date" => "5月15日",
                "place" => "オンライン",
                "description" => "楽しい大会にしましょう！"
            ],
        ],
        (object)[
            "deck" => (object)[
              "name" => "フローラトルネコ",
              "leader" => "トルネコ",
              "type" => "アグロ",
              "win_percent" => "90",
              "win_number" => "10000",
              "strong" => "ゼシカ",
            ],
            "compe" => (object)[
                "name" => "えだまめの大会",
                "style" => "トーナメント",
                "battle" => "BO3",
                "date" => "5月15日",
                "place" => "オンライン",
                "description" => "楽しい大会にしましょう！"
            ],
        ]
    ];

    return view('index',["datas"=>$datas]);
  }

}
