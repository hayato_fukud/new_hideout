<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Game;
use App\Leader;
use App\Format;
use App\Http\Requests\GameRequest;

class gameController extends Controller
{
    //
    public function gameRegister(Request $request){
        //user_idの取得
        $user_id = Auth::id();
        //nameとdetailのセッションを持ってた場合、nameとdetailを取得する
        $name = ($request->session()->get("name")) ? $request->session()->get("name") : "";
        $detail = ($request->session()->get("detail")) ? $request->session()->get("detail") : "";
        //次のhttp通信まで今までのflashデータを保存したままにする
        $request->session()->reflash();
        return view("mypage.game.register.game",compact("name","detail","user_id"));
    }

    public function leaderRegister(Request $request){
        //flashでnameとdetailを保存（もしnameとdetailを取得したなら）
        if($request->name && $request->detail){
            $request->session()->flash('name',$request->name);
            $request->session()->flash('detail',$request->detail);
        }
        //leadersのsessionを持っていた場合、leadersを取得する
        $leaders = ($request->session()->get("leaders")) ? $request->session()->get("leaders") : [];
        //次のhttp通信まで今までのflashデータを保存したままにする
        $request->session()->reflash();
        return view("mypage.game.register.leader",compact("leaders"));
    }

    public function formatRegister(Request $request){
        //formatsのsessionを持っていた場合、formatsを取得する
        $formats = ($request->session()->get("formats")) ? $request->session()->get("formats") : [];
        //次のhttp通信まで今までのflashデータを保存したままにする
        $request->session()->reflash();
        return  view('mypage.game.register.format',compact("formats"));
    }

    public function confirm(Request $request){
        $name = $request->session()->get("name");
        $detail = $request->session()->get("detail");
        $leaders = $request->session()->get('leaders');
        $formats = $request->session()->get('formats');
        $request->session()->reflash();
        return view('mypage.game.register.confirm',compact("name","detail","leaders","formats"));
    }

    public function gameStore(Request $request){
        //利用するDBの定義
        $db_game = new Game;
        $db_leader = new Leader;
        $db_format = new Format;
        //ゲーム情報を登録して、登録した場所のidを取得する
        $stored_game_id = $db_game->storeGame($request);

        $db_leader->storeLeader($request,$stored_game_id);
        $db_format->storeFormat($request,$stored_game_id);

        $request->session()->flash("success_message","ゲーム名の登録が完了しました。");
        return redirect("/mypage/game/list");
    }

    public function gameEdit(Request $request){
        $user_id = Auth::id();
        $db_game = new Game;
        $game = $db_game->getEditGame($request->game_id);
        \Log::info($game);
        return view('mypage.game.edit.edit_game',compact('game','user_id'));
    }

    public function leaderEdit(Request $request){
        //リーダー情報の取得
        $db_game = new Game;
        $db_leader = new Leader;
        $game_id = $request->game_id;
        $leaders = $db_leader->getLeader($game_id);
        return view('mypage.game.edit.edit_leader',compact("leaders","game_id"));
    }

    public function formatEdit(Request $request){
        $db_game = new Game;
        $db_format = new Format;
        $game_id = $request->game_id;
        $formats = $db_format->getFormat($game_id);
        return view('mypage.game.edit.edit_format',compact("formats","game_id"));
    }

    public function gameEdited(Request $request){
        $db_game = new Game;
        $game = $db_game->edit($request);
        //フラッシュデータの作成
        $request->session()->flash("success_message","ゲーム情報の編集が完了しました。");
        return redirect("/mypage/game/list");
    }

    public function leaderEdited(Request $request){
        $db_leader = new Leader;
        $db_leader->editLeader($request);
        //フラッシュデータの作成
        $request->session()->flash("success_message","リーダー情報の編集が完了しました。");
    }

    public function formatEdited(Request $request){
        $db_format = new Format;
        $db_format->editFormat($request);
        //フラッシュデータの作成
        $request->session()->flash("success_message","フォーマット情報の編集が完了しました。");
    }

    public function gameList(Request $request){
        //ゲーム情報、リーダー情報、フォーマット情報の取得
        $user_id = Auth::id();
        $db_game = new Game;
        $db_leader = new Leader;
        $db_format = new Format;
        $games = $db_game->searchGameList("",$user_id);
        foreach($games as $game){
            $game->leaders = $db_leader->getLeader($game->id);
            $game->formats = $db_format->getFormat($game->id);
        }
        //フラッシュメッセージの取得
        $alert = $request->session()->get("success_message");
        return view("mypage.game.list",compact('games','user_id','alert'));
    }

}
