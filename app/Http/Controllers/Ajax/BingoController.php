<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Bingo;

class BingoController extends Controller
{
    //
    public function getImage(){
        //bingoデータの取得
        $data = Bingo::all();
        //bingoデータの並び替え
        for($i = count($data) - 1 ; $i >= 0 ; $i--){
            $j = rand(0,count($data) - 1);
            // \Log::info($j);
            $tmp = $data[$i];
            $data[$i] = $data[$j];
            $data[$j] = $tmp;
        }
        //返す
        return $data;
    }
}
