<?php

namespace App\Http\Controllers\Ajax;

use App\Game;
use App\Leader;
use App\Format;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GameController extends Controller
{
    //

    public function changeGame(Request $request){
        $db_game = new Game;
        $db_leader = new Leader;
        $db_format = new Format;
        $db_game->changeChoice($request->game_id,$request->user_id);
        $games = $db_game->searchGameList("",$request->user_id);
        foreach($games as $game){
            $game->leaders = $db_leader->getLeader($game->id);
            $game->formats = $db_format->getFormat($game->id);
        }
        return $games;
    }

    public function searchGame(Request $request){
        $db_game = new Game;
        return $db_game->searchGameList($request->game_name,$request->user_id);
    }

    public function deleteGame(Request $request){
        $db_game = new Game;
        $db_leader = new Leader;
        $db_format = new Format;
        $db_game->deleteGame($request->game_id);
        $games = $db_game->searchGameList("",$request->user_id);
        foreach($games as $game){
            $game->leaders = $db_leader->getLeader($game->id);
            $game->formats = $db_format->getFormat($game->id);
        }
        return $games;
    }

    //ゲーム情報登録系
    public function sessionStoreGame(Request $request){
        //flashでgameデータを保存
        $request->session()->flash('name',$request->name);
        $request->session()->flash('detail',$request->detail);
        //次のhttp通信まで今までのflashデータを保存したままにする
        $request->session()->reflash();
    }

    public function sessionStoreLeader(Request $request){
        //flashでleadersを保存
        $request->session()->flash('leaders',$request->leaders);
        //次のhttp通信まで今までのflashデータを保存したままにする
        $request->session()->reflash();
    }

    public function sessionStoreFormat(Request $request){
        //flashでformatsを保存
        $request->session()->flash('formats',$request->formats);
        //次のhttp通信まで今までのflashデータを保存したままにする
        $request->session()->reflash();
    }

    //ゲーム編集関連系
    public function editedGame(Request $request){
        $db_game = new Game;
        $game = $db_game->edit($request);
        //フラッシュデータの作成
        $request->session()->flash("success_message","ゲーム情報の編集が完了しました。");
    }

}
