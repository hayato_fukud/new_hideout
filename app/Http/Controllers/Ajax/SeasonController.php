<?php

namespace App\Http\Controllers\ajax;

use App\Season;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SeasonController extends Controller
{
    //
    public function searchSeason(Request $request){
        $db_season = new Season;
        return $db_season->searchSeasonList($request->search_season,$request->user_id);
    }

    public function deleteSeason(Request $request){
        $db_season = new Season;
        $db_season->deleteSeason($request->season_id);
        $new_season = $db_season->searchSeasonList("",$request->user_id);
        return $new_season;
    }
}
