<?php

namespace App\Http\Controllers\Ajax;

use Auth;
use App\Deck;
use App\Game;
use App\Opponent;
use App\Character;
use App\RepresentativeDeck;
use App\Services\BattleHistoryService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeckdataController extends Controller
{
    use SoftDeletes;

    protected $battleHistoryService;

    public function __construct(BattleHistoryService $battle_history_service)
    {
        $this->battleHistoryService = $battle_history_service;
    }

    public function userdeck(){
      $user = Auth::user();
      $decks = Deck::where('user_id',$user->id)->get();
      return $decks;
    }

    public function alldeck(){
      return Deck::all();
    }

    public function getAllBattleData(Request $request){
        $data = $this->battleHistoryService->editAllBattleHistory($request->season_id,$request->format_id);
        return $data;
    }

    //デッキ詳細で使うajax
    // public function getDeck(){
    //   return Deck::where('id',session("deck_id"))->first();
    // }

    // public function getOpponents(){
    //   return Opponent::where('deck_id',session("deck_id"))->get();
    // }

    // public function getLeader(){
    //   return Character::where('name',session("deck_leader"))->first();
    // }

    public function deckdetaildata(Request $request){
        $opponent = new Opponent;
        return $opponent->getOpponentTableData($request->deck_id);
    }

    public function registerRepresentativeDeck(Request $request){
        $rep_deck = RepresentativeDeck::where("leader_id",$request->leader_id)
          ->where("deck_id",$request->deck_id)->first();
        \Log::info($rep_deck);
        if(empty($rep_deck)){
            $rep_deck = new RepresentativeDeck;
            $rep_deck->representative_deck_name = $request->content;
            $rep_deck->deck_id = $request->deck_id;
            $rep_deck->leader_id = $request->leader_id;
            $rep_deck->save();
        }else{
            $rep_deck->representative_deck_name = $request->content;
            $rep_deck->save();
        }
    }


    public function registerMemo(Request $request){
        $rep_memo = RepresentativeDeck::where("leader_id",$request->leader_id)
          ->where("deck_id",$request->deck_id)->first();
        \Log::info($rep_memo);
        if(empty($rep_memo)){
            $rep_memo = new RepresentativeDeck;
            $rep_memo->memo = $request->content;
            $rep_memo->deck_id = $request->deck_id;
            $rep_memo->leader_id = $request->leader_id;
            $rep_memo->save();
        }else{
            $rep_memo->memo = $request->content;
            $rep_memo->save();
        }
    }

    public function editOpponentDeck(Request $request){
        // デッキデータをオブジェクトにする。
        $edit_deck = (object) $request->edit;
        $edit_deck->deck_id = $request->deck_id;

        //対戦データを取得
        $opponent = Opponent::where("id",$edit_deck->id)->first();

        // 編集前の対戦データをbattlehistoryテーブルから削除する
        $this->battleHistoryService->deleteBattleHistoryData($edit_deck,$opponent);

        //opponentテーブルに値を上書き保存
        $opponent->deck_name = $edit_deck->deck_name;
        $opponent->leader_id = $edit_deck->leader_id;
        $opponent->win_or_lose = $edit_deck->win_or_lose;
        $opponent->order = $edit_deck->order;
        $opponent->memo = $edit_deck->memo;
        $opponent->save();

        //battlehistoryテーブルの値を変更
        $this->battleHistoryService->editBattleHistoryData($edit_deck);
    }

    public function deleteOpponentDeck(Request $request){
        $opponent = Opponent::where("id",$request->del_opponent_deck_id)->first();
        $this->battleHistoryService->deleteBattleHistoryData($request,$opponent);
        $opponent->delete();
        // $db_opponent = new Opponent;
        // $db_opponent->deleteOpponentDeck($request->delete);
    }


}
