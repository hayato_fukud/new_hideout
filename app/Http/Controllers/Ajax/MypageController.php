<?php

namespace App\Http\Controllers\Ajax;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Deck;

class MypageController extends Controller
{
    //
    public function registerFavorite(Request $request){
        //変更前のお気に入りデッキを編集
        $before_favorite_deck = Deck::where("favorite_deck_id",1)
                                  ->where("season_id",$request->season_id)->first();
        if(!empty($before_favorite_deck)){
            $before_favorite_deck->favorite_deck_id = 0;
            $before_favorite_deck->save();
        };
        //変更後のお気に入りデッキを編集
        $after_favorite_deck = Deck::where("id",$request->deck_id)->first();
        $after_favorite_deck->favorite_deck_id = 1;
        $after_favorite_deck->save();
    }

    public function getUserId(){
        $id = Auth::id();
        return $id;
    }
}
