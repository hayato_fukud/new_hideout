<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MinigameController extends Controller
{
    public function index(){
      return view('minigame.index');
    }

    public function bingo(){
        return view("minigame.gamelist.bingo");
    }
}
