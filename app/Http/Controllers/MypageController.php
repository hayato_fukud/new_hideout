<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use App\Deck;
use App\Opponent;
use App\Character;
use App\Game;
use App\Leader;
use App\Format;
use App\Season;
use App\BattleHistory;
use App\Services\BattleHistoryService;
use App\Http\Requests\DeckRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;

class MypageController extends Controller
{

    protected $battleHistoryService;

    public function __construct(BattleHistoryService $battle_history_service)
    {
        $this->battleHistoryService = $battle_history_service;
    }

    //トップページ
    public function index(Request $request){
        $user = Auth::user();
        $db_deck = new Deck;
        $db_game = new Game;
        $db_season = new Season;
        $db_format = new Format;

        // $decks = [];
        $game = $db_game->getGame($user->id);
        $decks = $db_deck->editDeck($user->id,$game->id,0);
        $favorite = $db_deck->editDeck($user->id,$game->id,1);
        $seasons = $db_season->getSeason($user->id,$game->id);
        $formats = $db_format->getFormat($game->id);
        //フラッシュデータの取得
        $alert = $request->session()->get("success_message");
        return view('mypage.index',compact("user","decks","favorite","game","seasons","formats","alert"));
    }

    //デッキ詳細ページ
    public function deckdetail(Request $request){
        $user = Auth::user();

        $db_opponent = new Opponent;
        $db_deck = new Deck;
        $db_leader = new Leader;
        $db_game = new Game;

        //deckデータの編集・取得
        $deck = $db_deck->getDeckDetail($request);
        //leaderデータの取得
        $game = $db_game->getGame($user->id);
        $leaders = $db_leader->getLeader($game->id);
        //opponentデータの取得
        $opponents = $db_opponent->getOpponentData($request);

        $battle_history = $this->battleHistoryService->editBattleHistory($leaders,$deck->id);

        //フラッシュデータの取得
        $alert = $request->session()->get("success_message");

        return view('mypage.deckdetail',compact("deck","leaders","opponents","battle_history","alert"));
    }

    //対戦デッキの登録
    public function registerOpponent(Request $request){
        \Log::info($request);
        $db_opponent = new Opponent;
        $db_opponent->opponentDeckRegister($request);
        $this->battleHistoryService->editBattleHistoryData($request);
        //再び元のページに戻る
        return redirect()->route('mypage.detail',['id'=>$request->deck_id]);
    }

    //デッキ登録系
    public function deckregister(){
        $user_id = Auth::id();

        $db_game = new Game;
        $db_season = new Season;
        $db_leader = new Leader;
        $db_format = new Format;

        $game = $db_game->getGame($user_id);
        $seasons = $db_season->getSeason($user_id,$game->id);
        $leaders = $db_leader->getLeader($game->id);
        $formats = $db_format->getFormat($game->id);

        return view('mypage.deck.register',compact("game","seasons","leaders","formats","user_id"));
    }

    public function deckstore(Request $request){
        $user_id = Auth::id();

        $db_game = new Game();
        $db_deck = new Deck();
        $db_battle_history = new BattleHistory();

        $game = $db_game->getGame($user_id);
        $deck = $db_deck->deckStore($request,$game->id,0);
        $db_battle_history->createBattleHistory($deck);
        //フラッシュデータの設定
        $request->session()->flash("success_message","デッキの登録が完了しました。");
        return redirect('/mypage/{{$user_id}}');
    }

    public function deckEdit(Request $request){
        $user_id = Auth::id();

        $db_game = new Game;
        $db_season = new Season;
        $db_leader = new Leader;
        $db_format = new Format;

        $game = $db_game->getGame($user_id);
        $seasons = $db_season->getSeason($user_id,$game->id);
        $leaders = $db_leader->getLeader($game->id);
        $formats = $db_format->getFormat($game->id);

        $deck = Deck::where('id',$request->id)->first();
        return view(
            'mypage.deck.edit',
            compact("deck","game","seasons","leaders","formats")
        );
    }

    public function deckedited(Request $request){
        $deck = Deck::where('id',$request->id)->first();
        $deck->deckStore($request,0,1);
        //フラッシュデータの設定
        $request->session()->flash("success_message","デッキの編集が完了しました。");
        return redirect("/mypage/detail/{$deck->id}");
    }

    public function deleteDeck(Request $request){
      $user_id = Auth::id();
      $db_deck = new Deck;
      $db_deck->deleteDeck($request);
      return redirect("/mypage/$user_id");
    }
    // 大会登録機能系
    // public function comperegister(){
    //   return view('mypage.comperegister');
    // }

    //大会管理
    // public function compemanagement(){
    //   return view('mypage.compemanagement');
    // }

    //オプション
    public function option(){
        return view('mypage.option');
    }

    //ユーザー編集機能系
    public function userEdit(){
        $user = User::where('id',Auth::id())->first();
        return view('mypage.useredit',compact("user"));
    }

    public function userEdited(Request $request){
        $user_id = Auth::id();
        $db_user = new User;
        $db_user->userEdited($request,$user_id);
        $request->session()->flash("success_message","ユーザー情報の変更が完了しました。");
        return redirect('/mypage/{{$user_id}}');
    }
}
