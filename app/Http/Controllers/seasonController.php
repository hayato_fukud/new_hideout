<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\BattleHistoryService;
use App\Http\Requests\SeasonRequest;
use App\Game;
use App\Season;
use App\Deck;


class seasonController extends Controller
{
    protected $battleHistoryService;

    public function __construct(BattleHistoryService $battle_history_service){
        $this->battleHistoryService = $battle_history_service;
    }

    public function seasonRegister(){
        $user_id = Auth::id();
        return view("mypage.season.register",compact("user_id"));
    }

    public function seasonStore(SeasonRequest $request){
        $db_season = new Season;
        $db_season->store($request);
        //フラッシュメッセージの登録
        $request->session()->flash("success_message","シーズン名の登録が完了しました。");
        return redirect("/mypage/season/list");
    }

    public function seasonEdit(Request $request){
        $user_id = Auth::id();
        $db_season = new Season;
        $season = $db_season->getEditSeason($request->season_id);
        return view('mypage.season.edit',compact('season',"user_id"));
    }

    public function seasonEdited(SeasonRequest $request){
        $db_season = new Season;
        $season = $db_season->edit($request);
        //フラッシュメッセージの表示
        $request->session()->flash("success_message","シーズン名の編集が完了しました。");
        return redirect("/mypage/season/list");
    }

    public function seasonList(Request $request){
        $user_id = Auth::id();
        $db_game =  new Game;
        $db_season = new Season;
        $db_deck = new Deck;

        //ゲームidの取得
        $game = $db_game->getGame($user_id);
        //シーズンリストの取得
        $list = $db_season->getSeasonList($game->id);
        //勝率の取得
        foreach($list as $value){
            $value->battle_data = $this->battleHistoryService->editAllBattleHistory($value->id,0);
        }
        //フラッシュメッセージの登録
        $alert = $request->session()->get("success_message");
        return view("mypage.season.list",compact('list','user_id','alert'));
    }
}
