<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SeasonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            "game" => ["required"],
            "name" => ["required"],
        ];
    }

    // public function messages(){
    //     return [
    //         'game.required'  => 'ゲーム名は必ず指定してください。',
    //     ];
    // }
}
