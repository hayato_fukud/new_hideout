<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Leader extends Model
{
    //
    public function storeLeader($request,$game_id){
        foreach($request->leaders as $leader){
            $this->insert([
                "leader_name" => $leader,
                "game_id" => $game_id
            ]);
        }
    }

    public function getLeader($game_id){
        return $this->where("game_id",$game_id)->get();
    }

    public function editLeader($request){
        foreach($request->leaders as $leader){
            //新規リーダーの登録の場合、新規登録できるようにする
            if($leader["id"] == 0){
                $this->leader_name = $leader["leader_name"];
                $this->game_id = $request->game_id;
                $this->save();
            }
            else{
                $db_leader = $this->where("id",$leader["id"])->first();
                $db_leader->leader_name = $leader["leader_name"];
                $db_leader->save();
            }
        }

        foreach($request->delete_leaders as $leader){
            $db_leaders = $this->where("id",$leader)->first();
            $db_leaders->delete();
        }
    }

    public function Leader(){
        return $this->hasMany("App\Deck","leader_id");
    }

    public function Game(){
        return $this->hasMany('App\Game',"game_id");
    }
}
