<?php

use App\Game;
use App\Season;

function getGameName(int $user_id){
    $html = "<option disabled selected>選択してください。</option>";
    $games = Game::where('user_id',$user_id)->get();
    foreach($games as $game){
        $html .= "<option value='".$game->id."'>".$game->game_name."</option>";
    }
    return $html;
}

function getEditGameName(int $user_id,int $season_id){
    $html = "<option disabled selected>選択してください。</option>";
    $games = Game::where('user_id',$user_id)->get();
    $season = Season::where("id",$season_id)->first();
    foreach($games as $game){
        \Log::info($game);
        \Log::info($season);
        if($season->game_id == $game->id){
            $html .= "<option value='".$game->id."' selected>".$game->game_name."</option>";
        }else{
            $html .= "<option value='".$game->id."'>".$game->game_name."</option>";
        }
    }
    return $html;
}
