<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Format extends Model
{
    //
    public function storeFormat($request,$game_id){
        foreach($request->formats as $format){
            $this->insert([
                "format_name" => $format,
                "game_id" => $game_id
            ]);
        }
    }

    public function getFormat($game_id){
        return $this->where("game_id",$game_id)->get();
    }

    public function editFormat($request){
        foreach($request->formats as $format){
            //新規リーダーの登録の場合、新規登録できるようにする
            if($format["id"] == 0){
                $this->format_name = $format["format_name"];
                $this->game_id = $request->game_id;
                $this->save();
            }
            else{
                $db_format = $this->where("id",$format["id"])->first();
                $db_format->format_name = $format["format_name"];
                $db_format->save();
            }
        }

        foreach($request->delete_formats as $format){
            $db_formats = $this->where("id",$format)->first();
            $db_formats->delete();
        }
    }

    public function Deck(){
        return $this->belongTo("App\Deck","id","format_id");
    }


}
