<?php

namespace App;

use App\Character;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Opponent extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'deck_name', 'leader_id', 'win_or_lose', 'order', 'memo'
    ];

    public function opponentDeckRegister($request){
        $this->fill($request->all());
        $this->deck_id = $request->deck_id;
        $this->save();
    }

    public function getOpponentData($request){
        return $this->where('deck_id',$request->id)->get();

        //はじめにまとめたいデータ
        // $datas = [];
        // $sum = [
        //     "win" => 0,
        //     "lose" => 0,
        //     "win_per" => 0,
        //     "senkou" => 0,
        //     "koukou" => 0,
        //     "senkou_per" => 0,
        //     "strong" => "",
        //     "wrong" => "",
        // ];
        // $charas = Character::get(["id","name","job","icon"]);
        //
        // foreach($this->opponents as $opponent){
        //     //合計値の配列
        //     // $sum["win"] = 0;
        //     // $sum["lose"] = 0;
        //     // $sum["win_per"] = 0;
        //     // $sum["senkou"] = 0;
        //     // $sum["koukou"] = 0;
        //     // $sum["senkou_per"] = 0;
        //     // //強いリーダー、弱いリーダーの表示
        //     // $sum["strong"]="";
        //     // $sum["wrong"]="";
        //     $strong=0;
        //     $wrong=100;
        //
        //     //各リーダーごとの処理
        //     foreach($charas as $chara){
        //         //キャラクター情報をまとめる
        //         $datas[$chara->id]["name"] = $chara->name;
        //         $datas[$chara->id]["job"] = $chara->job;
        //         $datas[$chara->id]["icon"] = $chara->icon;
        //
        //         //各リーダーごとに必要なデータをまとめる
        //         $win = $this->where('deck_id',$request->id)->where('victory','勝利')
        //           ->where('leader',$chara->job)->get();
        //         $lose = $this->where('deck_id',$request->id)->where('victory','敗北')
        //           ->where('leader',$chara->job)->get();
        //         $senkou = $this->where('deck_id',$request->id)->where('play','先攻')
        //           ->where('leader',$chara->job)->get();
        //         $koukou = $this->where('deck_id',$request->id)->where('play','後攻')
        //           ->where('leader',$chara->job)->get();
        //
        //         $datas[$chara->id]["win"] = count($win);
        //         $datas[$chara->id]['lose'] = count($lose);
        //         if(count($win) == 0){
        //             $datas[$chara->id]['win_per'] = 0;
        //         }else{
        //             $datas[$chara->id]['win_per'] = count($win) / ( count($win) + count($lose) ) * 100;
        //         };
        //         $datas[$chara->id]["senkou"] = count($senkou);
        //         $datas[$chara->id]['koukou'] = count($koukou);
        //         if(count($senkou) == 0){
        //             $datas[$chara->id]['senkou_per'] = 0;
        //         }else{
        //             $datas[$chara->id]['senkou_per'] = count($senkou) / ( count($senkou) + count($koukou) ) * 100;
        //         };
        //         //合計値に代入
        //         $sum["win"] += $datas[$chara->id]["win"];
        //         $sum["lose"] += $datas[$chara->id]["lose"];
        //         $sum["senkou"] += $datas[$chara->id]["senkou"];
        //         $sum["koukou"] += $datas[$chara->id]["koukou"];
        //
        //         if( $datas[$chara->id]['win_per']  >= $strong ){
        //           if( $datas[$chara->id]['win_per']  == $strong){
        //             $sum['strong'] = $sum['strong'] . "," . $datas[$chara->id]["job"];
        //           }else{
        //             $strong = $datas[$chara->id]['win_per'];
        //             $sum['strong'] = $datas[$chara->id]["job"];
        //           }
        //         }
        //
        //         if( $datas[$chara->id]['win_per']  <= $wrong ){
        //           if( $datas[$chara->id]['win_per']  == $wrong){
        //             $sum['wrong'] = $sum['wrong'] . "," . $datas[$chara->id]["job"];
        //           }else{
        //             $wrong = $datas[$chara->id]['win_per'];
        //             $sum['wrong'] = $datas[$chara->id]["job"];
        //           }
        //         }
        //     };
        //
        //     //合計値のパーセントを表示
        //     $sum["win_per"] = $sum["win"] / ( $sum["win"] + $sum["lose"] ) * 100;
        //     $sum["senkou_per"] = $sum["senkou"] / ( $sum["senkou"] + $sum["koukou"] ) * 100;
        //
        // }
        // if(empty($datas)){
        //     foreach($charas as $chara){
        //         //キャラクター情報をまとめる
        //         $datas[$chara->id]["name"] = $chara->name;
        //         $datas[$chara->id]["job"] = $chara->job;
        //         $datas[$chara->id]["icon"] = $chara->icon;
        //         //各リーダーごとに必要なデータをまとめる
        //         $datas[$chara->id]["win"] = 0;
        //         $datas[$chara->id]['lose'] = 0;
        //         $datas[$chara->id]['win_per'] = 0;
        //         $datas[$chara->id]["senkou"] = 0;
        //         $datas[$chara->id]['koukou'] = 0;
        //         $datas[$chara->id]['senkou_per'] = 0;
        //     };
        // }
        //
        // $this->datas = $datas;
        // $this->sum = $sum;
    }


    public function getOpponentTableData($deck_id){
        \Log::info($deck_id);
        return $this->where('deck_id', $deck_id)->orderBy('created_at','desc')->get();
    }

    public function deleteOpponentDeck($deck_id){
        $opponent = $this->where("id",$deck_id)->first();
        $opponent->delete();
    }
}
        //はじめにまとめたいデータ
    //     $datas = [];
    //     $charas = Character::get(["id","name","job","icon"]);
    //
    //     foreach($this->opponents as $opponent){
    //         //合計値の配列
    //         $sum["win"] = 0;
    //         $sum["lose"] = 0;
    //         $sum["win_per"] = 0;
    //         $sum["senkou"] = 0;
    //         $sum["koukou"] = 0;
    //         $sum["senkou_per"] = 0;
    //         //強いリーダー、弱いリーダーの表示
    //         $sum["strong"]="";
    //         $sum["wrong"]="";
    //         $strong=0;
    //         $wrong=100;
    //
    //         //各リーダーごとの処理
    //         foreach($charas as $chara){
    //             //キャラクター情報をまとめる
    //             $datas[$chara->id]["name"] = $chara->name;
    //             $datas[$chara->id]["job"] = $chara->job;
    //             $datas[$chara->id]["icon"] = "/image/icon/" . $chara->icon;
    //
    //             //各リーダーごとに必要なデータをまとめる
    //             $win = $this->where('deck_id',session("deck_id"))->where('victory','勝利')
    //               ->where('leader',$chara->job)->get();
    //             $lose = $this->where('deck_id',session("deck_id"))->where('victory','敗北')
    //               ->where('leader',$chara->job)->get();
    //             $senkou = $this->where('deck_id',session("deck_id"))->where('play','先攻')
    //               ->where('leader',$chara->job)->get();
    //             $koukou = $this->where('deck_id',session("deck_id"))->where('play','後攻')
    //               ->where('leader',$chara->job)->get();
    //
    //             $datas[$chara->id]["win"] = count($win);
    //             $datas[$chara->id]['lose'] = count($lose);
    //             if(count($win) == 0){
    //                 $datas[$chara->id]['win_per'] = 0;
    //             }else{
    //                 $datas[$chara->id]['win_per'] = count($win) / ( count($win) + count($lose) ) * 100;
    //             };
    //             $datas[$chara->id]["senkou"] = count($senkou);
    //             $datas[$chara->id]['koukou'] = count($koukou);
    //             if(count($senkou) == 0){
    //                 $datas[$chara->id]['senkou_per'] = 0;
    //             }else{
    //                 $datas[$chara->id]['senkou_per'] = count($senkou) / ( count($senkou) + count($koukou) ) * 100;
    //             };
    //             //合計値に代入
    //             $sum["win"] += $datas[$chara->id]["win"];
    //             $sum["lose"] += $datas[$chara->id]["lose"];
    //             $sum["senkou"] += $datas[$chara->id]["senkou"];
    //             $sum["koukou"] += $datas[$chara->id]["koukou"];
    //
    //             if( $datas[$chara->id]['win_per']  >= $strong ){
    //               if( $datas[$chara->id]['win_per']  == $strong){
    //                 $sum['strong'] = $sum['strong'] . "," . $datas[$chara->id]["job"];
    //               }else{
    //                 $strong = $datas[$chara->id]['win_per'];
    //                 $sum['strong'] = $datas[$chara->id]["job"];
    //               }
    //             }
    //
    //             if( $datas[$chara->id]['win_per']  <= $wrong ){
    //               if( $datas[$chara->id]['win_per']  == $wrong){
    //                 $sum['wrong'] = $sum['wrong'] . "," . $datas[$chara->id]["job"];
    //               }else{
    //                 $wrong = $datas[$chara->id]['win_per'];
    //                 $sum['wrong'] = $datas[$chara->id]["job"];
    //               }
    //             }
    //         };
    //
    //         //合計値のパーセントを表示
    //         $sum["win_per"] = $sum["win"] / ( $sum["win"] + $sum["lose"] ) * 100;
    //         $sum["senkou_per"] = $sum["senkou"] / ( $sum["senkou"] + $sum["koukou"] ) * 100;
    //
    //     }
    //     $this->datas = $datas;
    //     $this->sum = $sum;
    // }
// }
