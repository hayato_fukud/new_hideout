<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BattleHistory extends Model
{
    //
    public function createBattleHistory($deck_id){
        $this->deck_id = $deck_id;
        $this->save();
    }

    public function getWinRate($deck_id){
        return $this->select("win_rate","win")->where("deck_id",$deck_id)->first();
    }

    public function editBattleHistoryData($request){
        $battle_history = $this->where("deck_id",$request->id)->first();

    }

}
