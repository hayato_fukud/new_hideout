<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeRepresentativeDecksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('representative_decks', function (Blueprint $table) {
            //
            $table->text("memo")->nullable()->after("representative_deck_name");
            $table->integer("deck_id")->after("memo");
            $table->integer("leader_id")->after("deck_id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('representative_decks', function (Blueprint $table) {
            //
        });
    }
}
