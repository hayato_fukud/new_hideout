<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBattleHistoriesTable extends Migration
{
    /**(
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('battle_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('deck_id');
            $table->integer('win');
            $table->integer('lose');
            $table->integer('win_rate');
            $table->integer('first');
            $table->integer('second');
            $table->integer('representative_deck_id');
            $table->text('memo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('battle_histories');
    }
}
