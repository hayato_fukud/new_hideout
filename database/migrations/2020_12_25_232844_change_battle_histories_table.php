<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeBattleHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('battle_histories', function (Blueprint $table) {
            //
            $table->integer('win')->default(0)->change();
            $table->integer('lose')->default(0)->change();
            $table->integer('win_rate')->default(0)->change();
            $table->integer('first')->default(0)->change();
            $table->integer('second')->default(0)->change();
            $table->integer('representative_deck_id')->default(0)->change();
            $table->text('memo')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('battle_histories', function (Blueprint $table) {
            //
        });
    }
}
