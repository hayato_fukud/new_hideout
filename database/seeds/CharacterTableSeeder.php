<?php

use Illuminate\Database\Seeder;

class CharacterTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('characters')->insert([
            [
                "job"=>"戦士",
                "name"=>"テリー",
                "icon"=>"icon_tery.png",
            ],
            [
                "job"=>"魔法使い",
                "name"=>"ゼシカ",
                "icon"=>"icon_kuso.png",
            ],
            [
                "job"=>"武闘家",
                "name"=>"アリーナ",
                "icon"=>"icon_arina.png",
            ],
            [
                "job"=>"僧侶",
                "name"=>"ククール",
                "icon"=>"icon_kuku.png",
            ],
            [
                "job"=>"商人",
                "name"=>"トルネコ",
                "icon"=>"icon_toru.png",
            ],
            [
                "job"=>"占い師",
                "name"=>"ミネア",
                "icon"=>"icon_minea.png",
            ],
            [
                "job"=>"魔剣士",
                "name"=>"ピサロ",
                "icon"=>"icon_pisaro.png",
            ],
            [
                "job"=>"盗賊",
                "name"=>"カミュ",
                "icon"=>"icon_kamyu.png",
            ],
        ]);
    }
}
