<?php

return [
    // 0:仮登録 1:本登録 2:メール認証済 9:退会済
    // 'USER_STATUS' => ['PRE_REGISTER' => '0', 'REGISTER' => '1', 'MAIL_AUTHED' => '2', 'DEACTIVE' => '9'],
    // 0:新規ユーザー登録時 1:新規ゲーム登録時 2:ゲーム編集時
    'Game_Register' => [
        'New_User' => '0',
        'New_Game' => '1',
        'Edit_Game' => '2'
    ],
    'ServiceName' => "Card Road",
    "Explain_Leader" => 'リーダーとは、ゲーム内でプレイヤーが操作できるキャラクターを指します。
                        以下の"リーダー追加ボタン"を押すことでリーダーを追加することができます。',
    "Explain_Format" => 'フォーマットとは、ゲーム内でのルール形式のことを指します。
                        以下の"フォーマット追加ボタン"を押すことでフォーマットを追加することができます。'
];
