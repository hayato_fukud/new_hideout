/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

Vue.component('list-of-decks', require('./components/ListOfDecksComponent.vue').default);

//ゲーム関連
Vue.component('list-of-games', require('./components/ListOfGamesComponent.vue').default);
Vue.component('game-form-component',require('./components/GamesFormComponent.vue').default);
Vue.component('leader-form-component', require('./components/LeaderFormComponent.vue').default);
Vue.component('format-form-component', require('./components/FormatFormComponent.vue').default);
Vue.component('leader-edit-form-component', require('./components/LeaderEditFormComponent.vue').default);
Vue.component('format-edit-form-component', require('./components/FormatEditFormComponent.vue').default);

Vue.component('list-of-seasons', require('./components/ListOfSeasonsComponent.vue').default);

Vue.component('battle-history-chart', require('./components/BattleHistoryChartComponent.vue').default);
Vue.component('battle-history-for-leader', require('./components/BattleHistoryForLeaderComponent.vue').default);
Vue.component('deck-detail', require('./components/DeckDetailComponent.vue').default);

//ゲーム作成関連
// Vue.component('bingo-top',require('./components/gamelist/bingo/BingoTop.vue').default);
Vue.component('bingo',require('./components/gamelist/bingo/Bingo.vue').default);

Vue.component('alert',require('./components/AlertComponent.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});
