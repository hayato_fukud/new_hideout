@extends('layouts.app')

@section('title','このサイトとは？')

@section('content')

    @include('layouts.header')

    <img class="w-100" src="{{ asset('image/title_what.png') }}" alt="">

    <div class="container">
        <h1 class="mt-5">底辺ゲーム実況者がなんとなくファンサイトを作ってみた</h1>
        <p>初めまして！<br>
          現在YouTubeのチャンネル登録者数270人の超ド底辺ゲーム実況者グループ「マメマメーズ」です！<br>
          「底辺なのに自分のファンサイト持ってたら面白いんじゃない？」そんな適当な考えから
          結構大変なサイト制作を始めてみました！<br>
          でも面白い人は気付いてるんじゃないでしょうか？「こんなサイト作ること何も面白くないぞ」って。<br>
          まぁそもそもこの理由すら適当ですから（笑）<br>
          でも面白さとサイトのクオリティは関係ありません！是非楽しんで見ていってくださいね！<br>
          そして、もしサイトが気に入ったという人は下のボタンからYouTubeのチャンネル登録お願いします！</p>
        <button class="btn btn-success" type="button" name="button">
            <i class="fa fa-youtube fa-2x mr-2" aria-hidden="true"></i>
            YouTubeへ！
        </button>
        <button class="btn btn-success" type="button" name="button">
            <i class="fa fa-twitter-square fa-2x mr-2" aria-hidden="true"></i>
            Twitterへ！
        </button>
        <h1 class="mt-5">このサイトでできることは？</h1>
        <p>このサイトではマメマメーズのファンが集まれる場所として開発を進めていく予定です！<br>
          いわゆるファンサイトってやつですね！人気ないのに…<br>
          現在はYouTubeの生放送で作ったミニゲームの公開と、ドラゴンクエストライバルズの
          戦績管理サービス「ライバルズの旅」を公開しています！<br>
          将来的にはマメマメーズの動画を楽しみながら使えるサービスとかにしこうかと思っています！</p>
        <h1 class="mt-5">マメマメーズと仲間たち</h1>
        <div class="row">
          <div class="col-6">
              <div class="card">
                  <div class="row">
                      <div class="col-6">
                          <img class="w-100" src="{{ asset('image/mamemame-zu.jpg') }}" alt="">
                      </div>
                      <div class="col-6 mx-0">
                          <div class="card-body">
                              <div class="card-title">マメマメーズ</div>
                              <div class="card-subtitle">チャンネル登録者269人</div>
                              <div class="card-text">
                                  主にドラクエ系のゲームとマリオ系のゲームを実況<br>
                                  うるさい系の実況者として騒ぎながらゲームをしている<br>
                                  さらにゲームが下手なので、うざい人がしにまくって絶望
                                  している姿を見たい人にはオススメの実況者グループ
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

        </div>
    </div>

    @include('layouts.footer')



@endsection
