@extends('layouts.app')

@section('title','新規登録')

@section('content')

  @include('layouts.header')
  <div class="card" width=100%>
      <img class="card-img" src="{{ asset('image/title.png') }}">
      <div class="card-img-overlay">
          <h1 class="text-center display-2 top_title">新規登録</h1>
      </div>
  </div>

  <div class="container py-5">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header text-center">
                      <h2 class="my-4">{{config('const.ServiceName')}}</h2>
                  </div>
                  <div class="card-body">
                      @include('error_card_list')
                      <h3>Step.1 ユーザー情報入力</h3>
                      <p>"{{config('const.ServiceName')}}"の利用は4ステップで完了します。</p>
                      <form class="py-3" method="post" action="/register/step2">
                          @csrf
                          <!-- ユーザー名 -->
                          <div class="form-group">
                              <label for="name">ユーザー名:</label><br>
                              <input type="text" name="name" class="form-control" require value="{{old('name')}}">
                          </div>

                          <!-- email -->
                          <div class="form-group">
                              <label for="email">メールアドレス:</label><br>
                              <input type="text" name="email" class="form-control" require value="{{old('email')}}">
                          </div>
                          <!-- パスワード -->
                          <div class="form-group">
                              <label for="password">パスワード:</label><br>
                              <input type="password" name="password" class="form-control">
                          </div>
                          <!-- パスワード確認 -->
                          <div class="form-group">
                              <label for="password_confilmation">パスワード（確認）:</label><br>
                              <input type="password" id="password_confirmation" name="password_confirmation" class="form-control">
                          </div>
                          <p class="text-right" style="font-size:12px;">パスワードは8文字以上で作成してください。</p>

                          <div class="text-center">
                              <a href="/login" class="btn btn-danger mt-3">ログイン画面へ</a>
                              <button type="submit" name="button" class="btn btn-success mt-3">次へ</button>
                              <!-- <div class="my-3">
                                  <a class="text-center" href="{{route('login')}}">ログインはこちらから</a>
                              </div> -->
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  @include('layouts.footer')

@endsection
