@extends('layouts.app')

@section('title','新規登録')

@section('content')

  @include('layouts.header')
  <div class="card" width=100%>
      <img class="card-img" src="{{ asset('image/title.png') }}">
      <div class="card-img-overlay">
          <h1 class="text-center display-2 top_title">新規登録</h1>
      </div>
  </div>

  <div class="container py-5">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">
                      <h3>登録確認</h3>
                      <p>"{{config('const.ServiceName')}}"への登録は以下の内容でよろしいでしょうか？</p>
                  </div>
                  <div class="card-body">
                      <form class="" action="/register" method="post">
                          @csrf
                          <h4>ユーザー情報</h4>
                          <div class="mx-5">
                              <div class="form-group row">
                                  <label for="name col-4">名前：</label>
                                  <p class="col-8">{{\old('name')}}</p>
                                  <input type="hidden" name="name" value="{{\old('name')}}">
                              </div>
                              <div class="form-group row">
                                  <label for="email col-4">メールアドレス：</label>
                                  <p class="col-8">{{\old('email')}}</p>
                                  <input type="hidden" name="email" value="{{\old('email')}}">
                              </div>
                              <div class="form-group row">
                                  <label for="password col-4">パスワード：</label>
                                  <p class="col-8">{{$password_mask}}</p>
                                  <input type="hidden" name="password" value="{{\old('password')}}">
                              </div>
                          </div>

                          <h4>ゲーム情報</h4>
                          <div class="mx-5">
                              <div class="form-group row">
                                  <label for="game_name col-4">ゲーム名：</label>
                                  <p class="col-8">{{$data['name']}}</p>
                                  <input type="hidden" name="game_name" value="{{$data['name']}}">
                              </div>
                              <div class="form-group row">
                                  <label for="game_detail col-4">ゲーム詳細：</label>
                                  <p class="col-8">{{$data['detail']}}</p>
                                  <input type="hidden" name="game_detail" value="{{$data['detail']}}">
                              </div>
                              <div class="form-group row">
                                  <label for="leaders col-4">リーダー名：</label>
                                  <div class="col-8">
                                      @foreach($data["leaders"] as $key => $leader)
                                          <p class="">{{$leader}}</p>
                                          <input type="hidden" name="leaders[{{$key}}]" value="{{$leader}}">
                                      @endforeach
                                  </div>
                              </div>
                              <div class="form-group row">
                                  <label for="formats col-4">フォーマット名：</label>
                                  <div class="col-8">
                                      @foreach($data["formats"] as $key => $format)
                                          <p class="">{{$format}}</p>
                                          <input type="hidden" name="formats[{{$key}}]" value="{{$format}}">
                                      @endforeach
                                  </div>
                              </div>
                          </div>

                          <div class="text-center">
                              <a href="/login" class="btn btn-danger">ログイン画面へ</a>
                              <button type="submit" name="button" class="btn btn-success">登録</button>
                          </div>
                      </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
  @include('layouts.footer')

@endsection
