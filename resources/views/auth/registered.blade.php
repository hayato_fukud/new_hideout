@extends('layouts.app')

@section('title','新規登録')

@section('content')

  @include('layouts.header')
  <div class="card" width=100%>
      <img class="card-img" src="{{ asset('image/title.png') }}">
      <div class="card-img-overlay">
          <h1 class="text-center display-2 top_title">新規登録</h1>
      </div>
  </div>

  <div class="container py-5">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header">
                      <h3>登録完了</h3>
                      <p>"{{config('const.ServiceName')}}"の登録が完了しました。</p>
                  </div>
                  <div class="card-body">
                      <p>この度は"{{config('const.ServiceName')}}"に登録していただきありがとうございます。<br>
                      引き続き"{{config('const.ServiceName')}}"を利用する際は以下からログインを行ってください。</P>
                      <a href="/login">ログイン画面へ移動>>></a>
                  </div>
              </div>
          </div>
      </div>
  </div>
  @include('layouts.footer')

@endsection
