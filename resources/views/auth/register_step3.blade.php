@extends('layouts.app')

@section('title','新規登録')

@section('content')

  @include('layouts.header')
  <div class="card" width=100%>
      <img class="card-img" src="{{ asset('image/title.png') }}">
      <div class="card-img-overlay">
          <h1 class="text-center display-2 top_title">新規登録</h1>
      </div>
  </div>

  <div class="container py-5">
      <div class="row justify-content-center">
          <div class="col-md-8">
              <div class="card">
                  <div class="card-header text-center">
                      <h2 class="my-4">{{config('const.ServiceName')}}</h2>
                  </div>
                  <div class="card-body">
                      <h3>Step.3 リーダー登録</h3>
                      <p>"{{config('const.ServiceName')}}"への登録は4ステップで完了します。</p>
                      <p>{{config('const.Explain_Leader')}}</p>
                      <leader-form-component
                        :get_leaders='{{json_encode($leaders)}}'
                        :flag="{{$flag}}">
                      </leader-form-component>
                  </div>
              </div>
          </div>
      </div>
  </div>
  @include('layouts.footer')

@endsection
