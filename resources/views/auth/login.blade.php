@extends('layouts.app')

@section('title','ログイン')

@section('content')

@include('layouts.header')

  <!-- <img class="w-100" src="{{ asset('image/title/rivals_journey/login.png') }}" alt=""> -->

  <div class="container py-5">
      <div class="row justify-content-center">
          <div class="col-md-6">
              <div class="card">
                  <div class="card-header text-center">
                      <h2 class="my-4">{{config('const.ServiceName')}}</h2>
                  </div>
                  <div class="card-body">
                      @include('error_card_list')
                      <h3 class="">ログイン</h3>
                      <!-- <p>"{{config('const.ServiceName')}}"を利用する場合はこちらからログインを行ってください。</p> -->
                      <form class="py-3" method="post" action="{{ route('login') }}">
                          @csrf
                          <div class="my-2">
                              <div class="form-group">
                                  <label for="email">Eメールアドレス:</label><br>
                                  <input type="text" name="email" class="form-control" value="{{old('email')}}">
                              </div>

                              <div class="">
                                  <label for="password">パスワード:</label><br>
                                  <input type="password" name="password" class="form-control">
                              </div>
                          </div>
                          <div class="text-center my-2">
                              <button type="submit" name="button" class="btn btn-success d-inline-block my-3">
                                ログイン
                              </button>
                              <div class="my-3">
                                  <a href="{{route('register')}}" class="d-inline-block">新規アカウント登録はこちらから</a><br>
                                  <a href="" class="d-inline-block">パスワードを忘れた人はこちらから</a>
                              </div>
                          </div>
                      </form>
                  </div>
              </div>
          </div>

      </div>


  </div>

@include('layouts.footer')

@endsection
