<div class="pt-2 footer" style="background-color:#39B54A;height:250px;" >
    <div class="container">
        <div id="footer_title" class="pt-3 d-flex justify-content-center">えだまめの秘密基地</div>
        <div class="d-flex justify-content-center row my-3">
            <div class="d-inline-block">
                <a class="mx-5 text-dark" href="https://twitter.com/EdaAdvenRec">
                    <i class="fa fa-twitter-square fa-3x" aria-hidden="true"></i>
                </a>
                <a class="mx-5 text-dark" href="https://www.youtube.com/channel/UCq3uZPAhkC3GDnVNAZJER2g?view_as=subscriber">
                    <i class="fa fa-youtube fa-3x" aria-hidden="true"></i>
                </a>
            </div>
        </div>

        <div class="d-flex justify-content-center">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb px-0" style="background-color:#39B54A">
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('home') }}">ホーム画面</a></li>
                    <li class="breadcrumb-item"><a class="text-dark" href="{{ route('what') }}">自作ミニゲーム</a></li>
                    @guest
                    <li class="breadcrumb-item"><a class="text-dark" href="{{route('login')}}">オリジナルサービス</a></li>
                    @endguest
                    @auth
                    <li class="breadcrumb-item"><a class="text-dark" href="{{route('mypage',['id' => Auth::user()->id])}}">オリジナルサービス</a></li>
                    @endauth
                </ol>
            </nav>
        </div>

    </div>
</div>
