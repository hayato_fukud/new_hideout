<div class="col-3 no-look-sm border-right border-dark">
    <div class="list-group list-group-flush" style="margin-top:70px;">
        <a class="list-group-item list-group-item-action"
          href="{{route('mypage',['id' => Auth::user()->id])}}">
            <i class="fas fa-home"></i>ホーム画面
        </a>
        <a class="list-group-item list-group-item-action"
          href="/mypage/game/list">
            <i class="fas fa-gamepad"></i>ゲーム一覧
        </a>
        <a class="list-group-item list-group-item-action"
          href="/mypage/season/list">
            <i class="fas fa-feather-alt"></i>シーズン一覧
        </a>
        <a class="list-group-item list-group-item-action"
          href="{{route('mypage.deckregister')}}">
            <i class="fas fa-plus-square"></i>新規デッキ登録
        </a>
        <a class="list-group-item list-group-item-action"
          href="/mypage/option">
            <i class="fas fa-cog"></i>オプション
        </a>
    </div>
</div>

<div class="d-sm-none d-block sp-display-menu w-100 border-top">
    <div class="row m-2">
        <a class="d-block col text-dark" style="padding:0px;"
          href="{{route('mypage',['id' => Auth::user()->id])}}">
            <i class="fas fa-home"></i>
            <p class="sm-menu-text">ホーム</p>
        </a>
        <a class="d-block col text-dark" style="padding:0px;"
          href="/mypage/game/list">
            <i class="fas fa-gamepad"></i>
            <p class="sm-menu-text">ゲーム一覧</p>
        </a>
        <a class="d-block col text-dark" style="padding:0px;"
          href="/mypage/season/list">
            <i class="fas fa-feather-alt"></i>
            <p class="sm-menu-text">シーズン一覧</p>
        </a>
        <a class="d-block col text-dark" style="padding:0px;"
          href="{{route('mypage.deckregister')}}">
            <i class="fas fa-plus-square"></i>
            <p class="sm-menu-text">デッキ登録</p>
        </a>
        <a class="d-block col text-dark" style="padding:0px;"
          href="/mypage/option">
            <i class="fas fa-cog"></i>
            <p class="sm-menu-text">オプション</p>
        </a>
        <!-- <div class="d-block col text-dark">
            <form method="post" action="/logout">
                @csrf
                <button type="submit" class="list-group-item list-group-item-action text-danger">
                    <i class="fas fa-sign-out-alt"></i>ログアウト
                </button>
            </form>
        </div> -->
        
    </div>
</div>