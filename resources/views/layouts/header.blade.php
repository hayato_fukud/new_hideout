<nav class="navbar navbar-expand-sm navbar-light" style="background-color:#39B54A">
    <a href="/" class="navbar-brand" style="font-size:25px;">えだまめの秘密基地</a>
    <!-- 各メニュー -->
    <button class="navbar-toggler" type="button"
      data-toggle="collapse"
      data-target="#navmenu1"
      aria-controls="navmenu1"
      aria-expanded="false"
      aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse ml-auto" id="navmenu1">
        <div class="navbar-nav">
            <!-- <a class="nav-item nav-link" href="">
                ブログ
            </a> -->
            <a class="nav-item nav-link" href="{{route('minigame')}}">
                ミニゲーム
            </a>
            @guest
            <a class="nav-item nav-link" href="{{route('login')}}">
                {{config("const.ServiceName")}}
            </a>
            @endguest
            @auth
            <a class="nav-item nav-link" href="{{route('mypage',['id' => Auth::user()->id])}}">
                {{config("const.ServiceName")}}
            </a>
            @endauth
        </div>
    </div>
</nav>


<!-- <nav class="navbar navbar-dark bg-dark"> -->
<!-- <nav class="navbar navbar-expand-sm navbar-light" style="background-color:#39B54A">
  <a href="#" class="navbar-brand">Navbar</a>
  <button class="navbar-toggler" type="button"
      data-toggle="collapse"
      data-target="#navmenu1"
      aria-controls="navmenu1"
      aria-expanded="false"
      aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navmenu1">
    <div class="navbar-nav">
      <a class="nav-item nav-link" href="#">Menu#1</a>
      <a class="nav-item nav-link" href="#">Menu#2</a>
      <a class="nav-item nav-link" href="#">Menu#3</a>
    </div>
  </div>
</nav> -->