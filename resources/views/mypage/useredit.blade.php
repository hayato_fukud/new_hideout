@extends('layouts.app')

@section('title','ユーザー情報編集')

@section('content')

@include('layouts.header')
<div class="card" width=100%>
    <img class="card-img" src="{{ asset('image/title/rivals_journey/user_edit.png') }}">
</div>

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9">
            <div class="mx-5 py-5">
                @include('error_card_list')
                <form method="post" action="/mypage/user/edited/{{$user->id}}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-12 col-sm-3">名前</label>
                        <input type="text" name="name" class="col-12 col-sm-9 form-control" value="{{$user->name}}">
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-12 col-sm-2">Eメール</label>
                        <input type="text" name="email" class="col-12 col-sm-9 form-control" value="{{$user->email}}">
                    </div>
                    <div class="form-group row">
                        <label for="icon" class="col-12 col-sm-2">画像</label>
                        @isset($user->icon)
                            <image class="col-12 col-sm-3" src="/storage/image/icon/{{$user->icon}}">
                        @endisset
                        @empty($user->icon)
                            <image class="col-12 col-sm-3" src="/image/unknown_person.jpg">
                        @endempty
                        <p class="offset-2 col-12 col-sm-9">現在設定中の画像</p>

                        <input type="file" name="icon" class="offset-sm-2 col-12 col-sm-9">
                    </div>

                    <div class="text-right">
                        <a href="" class="btn btn-info m-2">パスワード変更</a>
                        <a href="" class="btn btn-danger m-2">戻る</a>
                        <button type="submit" name="button" class="btn btn-success m-2">登録</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include("layouts.footer")

@endsection
