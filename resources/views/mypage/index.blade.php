@extends('layouts.app')

@section('title','マイページ')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/mypage.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            <div class="">
                <list-of-decks
                  :user='{!! json_encode($user) !!}'
                  :decks='{!! json_encode($decks) !!}'
                  :favorite='{{json_encode($favorite)}}'
                  :game='{{json_encode($game)}}'
                  :seasons='{{json_encode($seasons)}}'
                  :formats='{{json_encode($formats)}}'
                  :alert='"{{$alert}}"'>
                </list-of-decks>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection
