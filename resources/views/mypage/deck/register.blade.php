@extends('layouts.app')

@section('title','マイページ')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_register.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            @include('error_card_list')
            <div class="mx-5 py-2">
                <p class="">設定中のゲーム：{{$game->game_name}}</p>
                <div class="text-right">
                    <a href="/mypage/game/list" class="text-right">設定中のゲームを変更>>></a>
                </div>
                <form method="post" action="{{route('mypage.deckregistered')}}">
                    @include('mypage.deck.form',["seasons" => $seasons, "leaders" => $leaders, "formats" => $formats])
                </form>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection
