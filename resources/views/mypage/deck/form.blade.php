@csrf

<div class="form-group row">
  <label for="season" class="col-12 col-sm-3 pl-0">シーズン:</label>
  <select name="season" class="form-control col-12 col-sm-9 w-50">
      <option disabled selected>選択してください。</option>
      @foreach($seasons as $key => $season)
      <option value="{{$season->id}}"
        @if(old("leader") === "{{$season->id}}") selected @endif>{{$season->season_name}}</option>
      @endforeach
  </select>
</div>
<div class="form-group row">
  <label for="name" class="col-12 col-sm-3">デッキ名:</label>
  <input type="text" name="name" class="form-control col-12 col-sm-9" value="{{$deck->name ?? old('name')}}">
</div>
<div class="form-group row">
  <label for="leader" class="col-12 col-sm-3">リーダー:</label>
  <select name="leader" class="form-control col-12 col-sm-9 w-50">
      <option disabled selected value>選択してください。</option>
      @foreach($leaders as $key => $leader)
      <option value="{{$leader->id}}"
        @if(old("leader") === "{{$leader->id}}") selected @endif>{{$leader->leader_name}}</option>
      @endforeach

  </select>
</div>
<div class="form-group row">
    <label for="url" class="col-12 col-sm-3">デッキURL:</label>
    <textarea name="url" rows="5" cols="80"
      class="form-control col-12 col-sm-9"></textarea>
    <div class="col-12 text-right">
        <p class="d-inline-block" style="font-size:12px;">
            ※デッキ作成はカードゲームの公式カードライブラリ等から作成してください。
        </p>
    </div>
</div>


<div class="form-group row">
    <label for="field" class="col-12 col-sm-3">フォーマット：</label><br>
    <div class="col-12 col-sm-9">
        @foreach($formats as $format)
        <div class="form-check mr-4">
            <input class="form-check-input" type="radio" name="field" value="{{$format->id}}"
              @if(old("field") === "{{$format->id}}") checked @endif>
            <label class="form-check-label" for="field">{{$format->format_name}}</label>
        </div>
        @endforeach
    </div>
</div>
<!-- <div class="form-group row">
    <label for="release" class="col-2">公開</label><br>
    <div class="form-check mr-4">
        <input class="form-check-input" type="radio" name="release" value="0"
          @if(old("release") === 0) checked @endif>
        <label class="form-check-label" for="release">する</label>
    </div>
    <div class="form-check mr-4">
        <input class="form-check-input" type="radio" name="release" value="1"
          @if(old("release") === 1) checked @endif>
        <label class="form-check-label" for="release">しない</label>
    </div>
</div> -->

<div class="text-right">
    <a href="/mypage/{{$user_id}}" class="btn btn-danger m-2">戻る</a>
    <button type="submit" name="button" class="btn btn-success m-2">登録</button>
</div>
