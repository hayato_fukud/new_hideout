@extends('layouts.app')

@section('title','マイページ')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_edit.png') }}" alt="">

<div class="container">
    <div class="row">
        @include("layouts.menu")
        <div class="col-12 col-sm-9 px-0">
            @include('error_card_list')
            <div class="mx-5 py-5">
                <p class="d-inline-block">設定中のゲーム：{{$game->game_name}}</p>
                <div class="text-right">
                    <a href="/mypage/game/list">設定中のゲームを変更>>></a>
                </div>
                <form method="post" action="{{route('mypage.deckedited',['id'=>$deck->id])}}">
                    @method('PATCH')
                    @include('mypage.deck.form1')
                </form>
            </div>
        </div>
    </div>

</div>

@include('layouts.footer')

@endsection
