@extends('layouts.app')

@section('title','マイページ')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_register.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            @include('error_card_list')
            <div class="mx-5 py-2">
                <div class="mb-4">
                    <h2 class="">Step.3 フォーマットの登録</h2>
                    <p class="ml-3">ゲーム登録は3ステップで行います。</p>
                </div>
                <format-form-component
                  :get_formats='{{json_encode($formats)}}'>
                </format-form-component>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection
