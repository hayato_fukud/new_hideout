@extends('layouts.app')

@section('title','マイページ')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_register.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            @include('error_card_list')
            <div class="mx-5 py-2">
                <div class="mb-4">
                    <h2 class="">ゲーム登録確認</h2>
                    <p class="ml-3">次の内容で登録してもよろしいでしょうか？。</p>
                    <form class="m-3" method="post" action="/mypage/game/registered">
                        @csrf
                        <div class="card p-2">
                            <div class="row m-2">
                                <label for="name" class="col-12 col-sm-4 small-title">名前：</label>
                                <div class="col-12 col-sm-8 small-detail">{{$name}}</div>
                                <input type="hidden" name="name" value="{{$name}}">
                            </div>
                            <div class="row m-2">
                                <label for="detail" class="col-12 col-sm-4 small-title">詳細：</label>
                                <div class="col-12 col-sm-8 small-detail">{{$detail}}</div>
                                <input type="hidden" name="detail" value="{{$detail}}">
                            </div>
                            <div class="row m-2">
                                <label for="leaders" class="col-12 col-sm-4 small-title">リーダー名：</label>
                                <div class="col-12 col-sm-8 small-detail">
                                    @foreach($leaders as $key => $leader)
                                        <div class="">{{$leader}}</div>
                                        <input type="hidden" name="leaders[{{$key}}]" value="{{$leader}}">
                                    @endforeach
                                </div>
                            </div>
                            <div class="row m-2">
                                <label for="formats" class="col-12 col-sm-4 small-title">フォーマット：</label>
                                <div class="col-12 col-sm-8 small-detail">
                                    @foreach($formats as $key => $format)
                                        <div class="">{{$format}}</div>
                                        <input type="hidden" name="formats[{{$key}}]" value="{{$format}}">
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="text-right">
                            <a href="/mypage/game/register/step3" class="btn btn-danger m-2">戻る</a>
                            <a href="/mypage/game/list" class="btn btn-danger m-2">ゲーム一覧へ</a>
                            <a href="/mypage/1" class="btn btn-danger m-2">メインメニューへ</a>
                            <button type="submit" name="button" class="btn btn-success m-2">登録</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection
