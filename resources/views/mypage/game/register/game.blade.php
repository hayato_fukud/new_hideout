@extends('layouts.app')

@section('title','マイページ')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_register.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            <div class="mx-5 py-2">
                <div class="mb-4">
                    <h2 class="">Step.1 ゲーム名の登録</h2>
                    <p class="ml-3">ゲームの登録は3ステップで行います。</p>
                </div>
                <game-form-component
                  :get_name="'{{$name}}'"
                  :get_detail="'{{$detail}}'"
                  :user_id="{{$user_id}}"
                  :flag="{{config('const.Game_Register.New_Game')}}">
                </game-form-component>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection
