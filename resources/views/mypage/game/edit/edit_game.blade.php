@extends('layouts.app')

@section('title','ゲームデータ編集 step.1')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/mypage.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            <div class="mx-5 py-2">
                <game-form-component
                  :get_game_id="{{$game->id}}"
                  :get_name="'{{$game->game_name}}'"
                  :get_detail="'{{$game->game_detail}}'"
                  :user_id="{{$user_id}}"
                  :flag="{{config('const.Game_Register.Edit_Game')}}">
                </game-form-component>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection
