@extends('layouts.app')

@section('title','マイページ')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_register.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            @include('error_card_list')
            <div class="mx-5 py-2">
                <form method="post" action='{{route("game.edited",["game_id"=>$game->id])}}'>
                    @csrf
                    <div class="form-group row">
                        <label for="name" class="col-2">ゲーム名:</label>
                        <input type="text" name="name" class="form-control col-10" value="{{$game->game_name}}">
                    </div>
                    <div class="form-group row">
                        <label for="detail" class="col-2">ゲーム詳細:</label>
                        <textarea name="detail" rows="8" cols="80"
                          class="form-control col-10">{{$game->game_detail}}</textarea>
                    </div>
                    <div class="text-right">
                        <a href="/mypage/game/list" class="btn btn-danger m-2">戻る</a>
                        <button type="submit" name="button" class="btn btn-success m-2">編集</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection
