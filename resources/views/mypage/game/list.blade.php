@extends('layouts.app')

@section('title','ゲーム一覧')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_register.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            <div class="mt-3 text-right">
                <a class="btn btn-primary mx-2" href="/mypage/season/list">シーズン一覧へ</a>
                <a class="btn btn-success mx-2" href="/mypage/game/register/step1">ゲーム登録</a>
            </div>
            <div class="">
                <list-of-games
                  :user_id='{{$user_id}}'
                  :list='{{json_encode($games)}}'
                  :alert="'{{$alert}}'">
                </list-of-games>
            </div>
        </div>
    </div>

</div>

@include('layouts.footer')

@endsection
