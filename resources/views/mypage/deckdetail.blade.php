@extends('layouts.app')

@section('title','デッキ詳細')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_detail.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9">
            <div class="no-look-sm">
                <div class="border-bottom border-dark row">
                    <div class="col-5 px-4 py-5">
                        <h2 class="d-inline-block">{{$deck->deck_name}}</h2>
                    </div>
                    <div class="col-3 px-2 py-5">
                        <div class="tags d-inline-block">{{$deck->format}}</div>
                        <div class="tags d-inline-block">{{$deck->leader}}</div>
                    </div>
                    <div class="col-4 list-group list-group-flush border-left border-dark">

                        <button class="list-group-item list-group-item-action"
                        data-toggle="modal" data-target="#reg_deck">
                            対戦デッキ登録
                        </button>
                        <button class="list-group-item list-group-item-action">
                            デッキ確認
                        </button>
                        <a class="list-group-item list-group-item-action"
                        href="/mypage/deck/edit/{{$deck->id}}">
                            <i class="fa fa-edit"></i>デッキ編集
                        </a>
                        <button class="list-group-item list-group-item-action text-danger border-bottom border-dark"
                        data-toggle="modal" data-target="#deck_delete">
                            <i class="fa fa-trash"></i>デッキ削除
                        </button>
                        <div class="modal fade" id="deck_delete" tabindex="-1"
                        role="dialog" aria-labelledby="label1" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="label1">デッキ削除</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        このデッキを削除してもよろしいですか？<br>（一度消したデータは戻りません。）
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-primary" data-dismiss="modal">戻る</button>
                                        <a href="/mypage/detail/delete/{{$deck->id}}" type="button" class="btn btn-danger">削除</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="no-look-pc">
                <div class="">
                    <div class="border-bottom border-dark row">
                        <div class="col-7 px-4 py-5">
                            <h2 class="d-inline-block">{{$deck->deck_name}}</h2>
                        </div>
                        <div class="col-5 px-2 py-5">
                            <div class="tags d-inline-block">{{$deck->format}}</div>
                            <div class="tags d-inline-block">{{$deck->leader}}</div>
                        </div>
                    </div>
                    <div class="border-bottom border-dark row">
                        <div class="list-group list-group-flush w-100">
                            <button class="list-group-item list-group-item-action"
                            data-toggle="modal" data-target="#reg_deck">
                                対戦デッキ登録
                            </button>
                            <button class="list-group-item list-group-item-action">
                                デッキ確認
                            </button>
                            <a class="list-group-item list-group-item-action"
                            href="/mypage/deck/edit/{{$deck->id}}">
                                <i class="fa fa-edit"></i>デッキ編集
                            </a>
                            <button class="list-group-item list-group-item-action text-danger border-bottom border-dark"
                            data-toggle="modal" data-target="#deck_delete">
                                <i class="fa fa-trash"></i>デッキ削除
                            </button>
                            <div class="modal fade" id="deck_delete" tabindex="-1"
                            role="dialog" aria-labelledby="label1" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="label1">デッキ削除</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            このデッキを削除してもよろしいですか？<br>（一度消したデータは戻りません。）
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">戻る</button>
                                            <a href="/mypage/detail/delete/{{$deck->id}}" type="button" class="btn btn-danger">削除</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- モーダル -->
            <div class="modal fade" id="reg_deck" tabindex="-1"
                role="dialog" aria-labelledby="label1" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="label1">デッキ登録</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form class="" action="/mypage/detail/registered/{{$deck->id}}" method="post">
                            @csrf
                            <div class="modal-body">
                                <input name="deck_id" type="hidden" value="{{$deck->id}}">
                                <div class="form-group">
                                    <label for="deck_name">デッキ名：</label>
                                    <input type="text" name="deck_name" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label for="leader_id">リーダー：</label>
                                    <select class="form-control" name="leader_id">
                                        <option disable selected>選択してください。</option>
                                        @foreach($leaders as $leader)
                                        <option value="{{$leader->id}}">{{$leader->leader_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="win_or_lose">勝敗：</label><br>
                                    <!-- <div class="btn-group btn-group-toggle col-10" data-toggle="buttons">
                                        <label class="btn btn-primary">
                                            <input type="radio" name="win_or_lose" autocomplete="off" value="0">勝利
                                        </label>
                                        <label class="btn btn-primary">
                                            <input type="radio" name="win_or_lose" autocomplete="off" value="1">敗北
                                        </label>
                                    </div> -->
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio"
                                            name="win_or_lose" id="win_or_lose_1" value="0">
                                        <label class="form-check-label" for="win_or_lose">勝利</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio"
                                            name="win_or_lose" id="win_or_lose_2" value="1">
                                        <label class="form-check-label" for="win_or_lose">敗北</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="order">先攻・後攻：</label>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio"
                                            name="order" id="order_1" value="0">
                                        <label class="form-check-label" for="order">先攻</label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio"
                                            name="order" id="order_2" value="1">
                                        <label class="form-check-label" for="order">後攻</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="memo">メモ：</label>
                                    <textarea name="memo" rows="8" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">閉じる</button>
                                <button type="submit" class="btn btn-success">登録</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-12 col-sm-7 p-5">
                    <h3>デッキ成績</h3>
                    <table class="table table-bordered table-sm border-secondary" style="table-layout:fixed;">
                        <tr>
                            <td style="width:40%">勝率</td>
                            <td style="width:60%">
                                {{json_encode($battle_history[1]["win_rate"])}}%
                                ({{json_encode($battle_history[1]["win"])}}勝{{json_encode($battle_history[1]["lose"])}}敗)
                            </td>
                        </tr>
                        <tr>
                            <td>先攻</td>
                            <td>{{json_encode($battle_history[1]["first"])}}回</td>
                        </tr>
                        <tr>
                            <td>後攻</td>
                            <td>{{json_encode($battle_history[1]["second"])}}回</td>
                        </tr>
                        <tr>
                            <td>得意</td>
                            <td>{{$battle_history[1]["strong"]}}</td>
                        </tr>
                        <tr>
                            <td>苦手</td>
                            <td>{{$battle_history[1]["wrong"]}}</td>
                        </tr>
                    </table>
                </div>
                <div class="col-12 col-sm-5 p-5">
                    <battle-history-chart
                      :leaders="{{$leaders}}"
                      :win_rate="{{json_encode($battle_history[1]["win_rate"])}}"
                      :battle_history="{{json_encode($battle_history[0])}}">
                    </battle-history-chart>
                </div>
            </div>
            <!-- 各リーダーの勝率等 -->
            <battle-history-for-leader
              :leaders="{{$leaders}}"
              :deck="{{$deck}}"
              :battle_history="{{json_encode($battle_history[0])}}"
              :alert="'{{$alert}}'">
            </battle-history-for-leader>
            <!-- 対戦データ -->
            <deck-detail
              :leaders="{{$leaders}}"
              :deck="{{$deck}}"
              :opponents="{{$opponents}}">
            </deck-detail>
        </div>
    </div>
</div>






@include('layouts.footer')

@endsection
