@extends('layouts.app')

@section('title','登録完了')

@section('content')

@include('layouts.header')
<div class="container">
    <h1>登録完了</h1>
    <p>以下のリンクからMyPageに戻れます！<p>
    <a href="{{route('mypage',['id'=>$id])}}">戻る</a>
</div>

@endsection
