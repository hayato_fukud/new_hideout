@extends('layouts.app')

@section('title','オプション')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_register.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            <ul class="list-group px-2 mt-4 mb-5">
                <a class="list-group-item list-group-item-action"
                  href="{{ route('mypage.useredit',['id'=>Auth::id()]) }}">
                    <i class="fa fa-edit"></i>ユーザー情報編集
                </a>
                <form method="post" action="/logout">
                    @csrf
                    <button type="submit" class="list-group-item list-group-item-action text-danger">
                        <i class="fas fa-sign-out-alt"></i>ログアウト
                    </button>
                </form>
            </ul>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection