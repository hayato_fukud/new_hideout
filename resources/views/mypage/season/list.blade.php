@extends('layouts.app')

@section('title','シーズン一覧')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_register.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            <div class="mt-3 text-right">
                <a class="btn btn-primary mx-2" href="/mypage/game/list">ゲーム一覧へ</a>
                <a class="btn btn-success mx-2" href="/mypage/season/register">シーズン登録</a>
            </div>
            <div class="">
                <list-of-seasons
                  :user_id='{{$user_id}}'
                  :list='{{json_encode($list)}}'
                  :alert="'{{$alert}}'">
                </list-of-seasons>
            </div>
        </div>
    </div>

</div>

@include('layouts.footer')

@endsection
