@extends('layouts.app')

@section('title','マイページ')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/rivals_journey/deck_register.png') }}" alt="">

<div class="container">
    <div class="row">
        @include('layouts.menu')
        <div class="col-12 col-sm-9 px-0">
            @include('error_card_list')
            <div class="mx-5 py-5">
                <form method="post" action="/mypage/season/registered">
                    @csrf
                    <div class="form-group row">
                        <label for="game" class="col-12 col-sm-3 pl-0">ゲーム名：<span class="text-danger" style="font-size:10px;">必須</span></label>
                        <select name="game" class="form-control col-12 col-sm-9">
                            {!! getGameName($user_id) !!}
                        </select>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-12 col-sm-3 pl-0">シーズン名:<span class="text-danger" style="font-size:10px;">必須</span></label>
                        <input type="text" name="name" class="form-control col-12 col-sm-9" value="">
                    </div>
                    <div class="form-group row">
                        <label for="detail" class="col-12 col-sm-3 pl-0">シーズン詳細:</label>
                        <textarea name="detail" rows="8" cols="80"
                          class="form-control col-12 col-sm-9"></textarea>
                    </div>
                    <div class="text-right">
                        <a href="/mypage/game/list" class="btn btn-danger m-2">戻る</a>
                        <button type="submit" name="button" class="btn btn-success m-2">登録</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection
