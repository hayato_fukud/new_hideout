@extends('layouts.app')

@section('title','マメマメーズのアジト')

@section('content')

@include('layouts.header')

<div class="text-center">
    <img src="{{asset('image/main_img.jpg')}}" class="w-75">
</div>
<div class="text-center px-3" style="background-color:#39B54A" w height=500px>
    <h1 class="pt-4">誰得すぎる自作サービス大集合</h1>
    <p class="pb-5">初心者エンジニアが自分の趣味全開で様々なサービスを公開！<br>
                    世の中の役に立ちそうなサービスから誰得サービスまで<br>
                    幅広くヘンテコなソフトを作成していきます！</p>
</div>
<div class="container">
    <div class="text-center">
        <div class="my-4">
            <h2>最新情報</h2>
            <div class="row justify-content-center">
                <ul class="list-group px-0 mx-auto col-11 col-sm-8">
                    <li class="list-group-item">
                        <span class="d-block text-danger">2月1日</span>
                        新サイト「えだまめの秘密基地」を公開
                    </li>
                    <li class="list-group-item">
                        <span class="d-block text-danger">ー</span>
                        ー
                    </li>
                    <li class="list-group-item">
                        <span class="d-block text-danger">ー</span>
                        ー
                    </li>
                </ul>
            </div>
        </div>

        <div class="my-4">
            <h2>最新のミニゲーム</h2>
            <div class="row">
                <div class="col-md-4">
                    <div class="card my-3">
                        <div class="card-header">
                            <h3>いらすとやビンゴ</h3>
                        </div>
                        <img class="card-img-top border-bottom" src="{{asset('image/irasutoya/top.png')}}"  alt="">
                        <div class="card-body">
                            <p>いらすとやの絵を使ってで白熱したビンゴ大会を楽しもう！</p>
                            <div class="text-right">
                                <a href="/minigame/bingo">ゲームはこちらから！>>></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a class="d-inline-block float-right" href="/minigame">他のミニゲームはこちらから！</a>
        </div>

        <div class="my-4 py-4">
            <h2>えだまめの動画</h2>
            <div class="m-2 mx-5 movie-wrap">
                <iframe src="https://www.youtube.com/embed/826gHT2dCDs"
                  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen></iframe>
            </div>
            <div class="m-2 mx-5 movie-wrap">
                <iframe src="https://www.youtube.com/embed/826gHT2dCDs"
                  frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                  allowfullscreen></iframe>
            </div>
            <a class="d-block float-right" href="https://www.youtube.com/channel/UCq3uZPAhkC3GDnVNAZJER2g">他の動画はこちらから！>>></a>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection()

<!-- 将来導入予定のもの -->
<!-- <h2>みんなのデッキ</h2>
<div class="row border border-dark justify-content-center">
    @foreach($datas as $data)
    <div class="card col-5 mx-3 my-3 px-0">
        <div id="" class="d-flex flex-row card-header text-white pt-4 pb-4" style="background-color:#29ABE2">
            <div class="title">{{$data->deck->name}}</div>
            <div class="">
                <div class="tags">{{$data->deck->leader}}</div>
                <div class="tags">{{$data->deck->type}}</div>
            </div>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card-subtitle card-font1">勝率</div>
                        <div class="ml-3 card-font2">{{$data->deck->win_percent}}%</div>
                    </div>
                    <div class="col-md-6">
                        <div class="card-subtitle card-font1">勝利数</div>
                        <div class="ml-3 card-font2">{{$data->deck->win_number}}勝</div>
                    </div>
                </div>
            </li>
            <li class="list-group-item">
                <p class="text-center mt-3 card-font1">{{$data->deck->strong}}に強いデッキ</p>
            </li>
            <li class="list-group-item text-center">
                <a href="" class="btn btn-success rounded">確認</a>
                <a href="" class="btn btn-primary rounded">詳細</a>
                <a href="" class="btn btn-danger rounded">削除</a>
            </li>
        </ul>
    </div>
    @endforeach
</div> -->

<!-- <div class="content">
    <h2>ライバルズ大会情報</h2>
    <div class="framework">
        @foreach($datas as $data)
        <div class="card mx-2 mb-4 float-left" style="width:28rem;">
            <div id="" class="d-flex flex-row card-header text-white pt-4 pb-4" style="background-color:#29ABE2">
                <div class="title">{{$data->compe->name}}</div>
                <div class="">
                    <div class="tags">{{$data->compe->style}}</div>
                    <div class="tags">{{$data->compe->battle}}</div>
                </div>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="card-subtitle card-font1">開催日</div>
                            <div class="ml-3 card-font2">{{$data->compe->date}}%</div>
                        </div>
                        <div class="col-md-6">
                            <div class="card-subtitle card-font1">開催方法</div>
                            <div class="ml-3 card-font2">{{$data->compe->place}}</div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item">
                    <p class="text-center mt-3 card-font1">{{$data->compe->description}}</p>
                </li>
                <li class="list-group-item text-center">
                    <a href="" class="btn btn-primary rounded">詳細を確認</a>
                    <a href="" class="btn btn-danger rounded">参加をやめる</a>
                </li>
            </ul>
        </div>
        @endforeach
    </div>
</div> -->


