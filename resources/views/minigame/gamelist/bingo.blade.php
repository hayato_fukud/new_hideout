@extends('layouts.app')

@section('title','ミニゲーム')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/minigate/top.png') }}" alt="">

<div class="container">
    <bingo></bingo>
</div>

@include('layouts.footer')

@endsection
