@extends('layouts.app')

@section('title','ミニゲーム')

@section('content')

@include('layouts.header')

<img class="w-100" src="{{ asset('image/title/minigate/top.png') }}" alt="">

<div class="container">
    <div class="row mt-5">
        <div class="col-12 col-sm-4">
            <div class="card my-3">
                <div class="card-header">
                    <h3>いらすとやビンゴ</h3>
                </div>
                <img class="card-img-top border-bottom" src="{{asset('image/irasutoya/top.png')}}"  alt="">
                <div class="card-body">
                    <p>いらすとやの絵を使ってで白熱したビンゴ大会を楽しもう！</p>
                    <div class="text-right">
                        <a href="/minigame/bingo">ゲームはこちらから！>>></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')

@endsection
