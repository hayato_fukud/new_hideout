<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//ミニゲームの際に使用
Route::get("/bingo/getdata","Ajax\BingoController@getImage");

Route::post('changegame','Ajax\GameController@changeGame');
Route::post('game/search','Ajax\GameController@searchGame');
Route::post('game/delete','Ajax\GameController@deleteGame');

//ゲーム登録時に使用
Route::post('game/session/game','Ajax\GameController@sessionStoreGame');
Route::post('game/session/leader','Ajax\GameController@sessionStoreLeader');
Route::post('game/session/format','Ajax\GameController@sessionStoreFormat');
Route::post('game/edited/step1','Ajax\GameController@editedGame');


Route::post('season/search','Ajax\SeasonController@searchSeason');
Route::post('season/delete','Ajax\SeasonController@deleteSeason');

//メイン画面関連
Route::post('/main/register/favoritedeck','Ajax\MypageController@registerFavorite');
Route::post('/main/battledata',"Ajax\DeckdataController@getAllBattleData");

//デッキ詳細関連
Route::post('/deckdetaildata/repregister',"Ajax\DeckdataController@registerRepresentativeDeck");
Route::post('/deckdetaildata/memoregister',"Ajax\DeckdataController@registerMemo");

Route::post('/deckdetaildata','Ajax\DeckdataController@deckdetaildata')->name("ajax.deckdetaildata");

Route::post('/deckdetaildata/editopponent','Ajax\DeckdataController@editOpponentDeck');
Route::post('/deckdetaildata/deleteopponent','Ajax\DeckdataController@deleteOpponentDeck');

//ユーザーidの取得
Route::get("/get/userid","Ajax\MyPageController@getUserId");