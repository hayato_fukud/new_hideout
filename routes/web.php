<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', 'IndexController@index')->name('home');

//このサイトとは？
Route::get('/what','WhatController@index')->name('what');

//ミニゲーム関連
Route::get('/minigame','MinigameController@index')->name('minigame');
Route::get('/minigame/bingo','MinigameController@bingo');

//登録関連
Auth::routes();
Route::post('/register/step2', 'Auth\RegisterController@registerStep2');
Route::get('/register/step3', 'Auth\RegisterController@registerStep3');
Route::get('/register/step4', 'Auth\RegisterController@registerStep4');
Route::get('/register/confirm', 'Auth\RegisterController@confirm');


// マイページ関連
Route::group(['middleware' => 'auth','prefix' => 'mypage'],function(){
  Route::get('/{id}','MypageController@index')->where('id', '[0-9]{1,}')->name('mypage');

  //デッキ詳細ページ
  Route::get('/detail/{id}','MypageController@deckdetail')->name('mypage.detail');
  Route::post('/detail/registered/{deck_id}','MypageController@registerOpponent')->name('mypage.opponent.registered');
  Route::get('/detail/delete/{id}','MypageController@deleteDeck');

  // デッキ登録関連
  Route::get('/deck/register','MypageController@deckregister')->name('mypage.deckregister');
  Route::post('/deck/registered','MyPageController@deckstore')->name('mypage.deckregistered');
  Route::get('/deck/edit/{id}','MyPageController@deckEdit')->name('mypage.deckedit');
  Route::patch('/deck/edited','MyPageController@deckedited')->name('mypage.deckedited');
  Route::delete('/deck/delete','MyPageController@deckdelete')->name('mypage.deckdelete');

  //ゲーム関連
  //新規ゲーム登録関連
  Route::get('/game/register/step1','gameController@gameRegister');
  Route::get('/game/register/step2','gameController@leaderRegister');
  Route::get('/game/register/step3','gameController@formatRegister');
  Route::get('/game/register/confirm','gameController@confirm');
  Route::post('/game/registered','gameController@gameStore');
  //既存ゲーム編集関連
  Route::get('/game/edit/step1/{game_id}','gameController@gameEdit');
  Route::get('/game/edit/step2/{game_id}','gameController@leaderEdit');
  Route::get('/game/edit/step3/{game_id}','gameController@formatEdit');
  // Route::get('/game/edit/{game_id}','gameController@gameEdit');
  Route::post('/game/edited/step1/{game_id}','gameController@gameEdited')->name('game.edited');
  Route::post('/game/edited/step2/{game_id}','gameController@leaderEdited');
  Route::post('/game/edited/step3/{game_id}','gameController@formatEdited');
  //ゲームリスト関連
  Route::get('/game/list','gameController@gameList');

  //シーズン登録
  Route::get('/season/register','seasonController@seasonRegister');
  Route::post('/season/registered','seasonController@seasonStore');
  //シーズン編集
  Route::get('/season/edit/{season_id}','seasonController@seasonEdit');
  Route::post('/season/edited/{season_id}','seasonController@seasonEdited')->name('season.edited');
  Route::get('/season/list','seasonController@seasonList');

  //オプション
  Route::get('/option','MypageController@option');
  //ユーザーデータ編集関連
  Route::get('/user/edit/{id}','MypageController@userEdit')->name('mypage.useredit');
  Route::post('/user/edited/{id}','MypageController@userEdited');

  //以下のコードは現在は使用しない
  // 大会登録関連
  // Route::get('/comperegister','MypageController@comperegister');
  //大会管理関連
  // Route::get('/compemanagement','MypageController@compemanagement');

});

//ajax関連のデータ
Route::get('ajax/userdeckdata','Ajax\DeckdataController@userdeck');
Route::get('ajax/alldeckdata','Ajax\DeckdataController@alldeck');
// Route::get('ajax/getdeck','Ajax\DeckdataController@getDeck');
// Route::get('ajax/getopponents','Ajax\DeckdataController@getOpponents');
// Route::get('ajax/getleader','Ajax\DeckdataController@getleader');
// Route::get('ajax/deckdetaildata','Ajax\DeckdataController@deckdetaildata')->name("ajax.deckdetaildata");
